shyft-docs
==========

The documentation part of the shyft-doc/sphinx is now moved to 
[Shyft](https://gitlab.com/shyft-os/shyft/doc/sphinx).

The current content are kept here for reference, so that we can transfer the
relevant parts into the main repository in an efficient and timely manner.

For documentation on coding and development, please check the project's
[GitLab Shyft repository](https://gitlab.com/shyft-os/shyft).


Project structure
-----------------

This project contains the folders:

- publication_sources: Collection of project related publications
- publication_sources/<name>: LaTex documents for a publication

Each package documentation will have own sub-section folders, if necessary. In these
folders "code" or "images" can be present, if such are used in that sub-section.


Contributing
------------

Can be done on the main project, shyft/doc structure.

[Shyft code repository](https://gitlab.com/shyft-os/shyft) itself, following the development cycle and principles of the main repository.

