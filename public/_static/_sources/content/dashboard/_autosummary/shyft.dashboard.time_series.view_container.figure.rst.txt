shyft.dashboard.time\_series.view\_container.figure
===================================================

.. automodule:: shyft.dashboard.time_series.view_container.figure

   
   .. rubric:: Classes

   .. autosummary::
   
      Figure
   
   .. rubric:: Exceptions

   .. autosummary::
   
      FigureError
   