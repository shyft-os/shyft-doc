shyft.dashboard.maps.map\_layer
===============================

.. automodule:: shyft.dashboard.maps.map_layer

   
   .. rubric:: Functions

   .. autosummary::
   
      find_tags_from_tooltips
   
   .. rubric:: Classes

   .. autosummary::
   
      MapLayer
      MapLayerType
   
   .. rubric:: Exceptions

   .. autosummary::
   
      MapLayerError
   