shyft.dashboard.examples.figure\_legend
=======================================

.. automodule:: shyft.dashboard.examples.figure_legend

   
   .. rubric:: Classes

   .. autosummary::
   
      FigureLegend
      MultiTsAdapterSine
      TsAdapterSine
   