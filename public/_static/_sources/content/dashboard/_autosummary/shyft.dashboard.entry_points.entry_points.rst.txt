shyft.dashboard.entry\_points.entry\_points
===========================================

.. automodule:: shyft.dashboard.entry_points.entry_points

   
   .. rubric:: Functions

   .. autosummary::
   
      parse_entry_points_for_apps
   