shyft.dashboard.base.hashable
=============================

.. automodule:: shyft.dashboard.base.hashable

   
   .. rubric:: Classes

   .. autosummary::
   
      Hashable
   