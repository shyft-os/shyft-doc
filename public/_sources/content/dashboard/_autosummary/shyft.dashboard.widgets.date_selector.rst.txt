shyft.dashboard.widgets.date\_selector
======================================

.. automodule:: shyft.dashboard.widgets.date_selector

   
   .. rubric:: Classes

   .. autosummary::
   
      DateSelector
   